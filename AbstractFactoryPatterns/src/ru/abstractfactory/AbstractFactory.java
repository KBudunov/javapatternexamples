package ru.abstractfactory;

public class AbstractFactory {

	public static void main(String[] args) {
		//����������� ������� == ������� ���� ������ ���������
		//��������� ����� == ������� ���� ���� �������
		
		//����������� ������� �������� � ���� ������ ��������� �������
		//1 � �������� ������� ���������� �������� Factory
		//2 ����������� ������� ����� ����������� �������� ���������� ���� ����/����
		//����� ��������� ����� ��� ������������ ���������� ��������� � ������� �� ���������� ������
		
		DeviceFactory deviceFactory = getDeviceFactoryByCountryCode("ENG");
		Mouse mouse = deviceFactory.getMouse();
		Keyboard keyboard = deviceFactory.getKeyboard();
		Tachpad tachpad = deviceFactory.getTachpad();
		
		mouse.click();
		mouse.dblclick();
		keyboard.print();
		tachpad.track(20, 10);
	}
	
	//��� �������������� ��������
	private static DeviceFactory getDeviceFactoryByCountryCode(String lang) {
		switch (lang) {
		case "RU":
			return new RuDeviceFactory();
		case "ENG":
			return new EngDeviceFactory();
		default:
			throw new RuntimeException("Unsupported country code " + lang);
		}
	}
}

//���������� ���������
interface Mouse{
	void click();
	void dblclick();
	void scroll(int derection);
}
interface Keyboard{
	void print();
	void println();
}
interface Tachpad{
	void track(int deltaX, int deltaY);
}


//��������� ������� (��������� ��� ��� � UML  � �� ����)
//��� � ���� ���� ����������� ������� ������ � ���� ����������(����� ����� �����)
interface DeviceFactory{
	Mouse getMouse();
	Keyboard getKeyboard();
	Tachpad getTachpad();
}




//������� ������� ����� ������������� ����������
//RU
class RuDeviceFactory implements DeviceFactory{
	//��������� ��������� ��������� ������� == ����������� �������
	//��� ��������� �����
	@Override
	public Mouse getMouse() {
		return new RuMouse();
	}
	//��� ��������� �����
	@Override
	public Keyboard getKeyboard() {
		return new RuKeyboard();
	}
	//��� ��������� �����
	@Override
	public Tachpad getTachpad() {
		return new RuTachpad();
	}
}
//ENG
class EngDeviceFactory implements DeviceFactory{
	//��������� ��������� ��������� ������� == ����������� �������
	//��� ��������� �����
	@Override
	public Mouse getMouse() {
		return new EngMouse();
	}
	//��� ��������� �����
	@Override
	public Keyboard getKeyboard() {
		return new EngKeyboard();
	}
	//��� ��������� �����
	@Override
	public Tachpad getTachpad() {
		return new EngTachpad();
	}
}




//���������� ���������� �����������
class RuMouse implements Mouse{
	@Override
	public void click() {System.out.println("������ ������");}
	@Override
	public void dblclick() {System.out.println("������� ������ ������");}
	@Override
	public void scroll(int derection) {	
		if(derection>0)
			System.out.println("������ �����");
		if(derection<0)
			System.out.println("������ ����");
		if(derection==0)
			System.out.println("�� ��������");
	}
}
class RuKeyboard implements Keyboard{
	@Override
	public void print() {System.out.println("�������� ������");}
	@Override
	public void println() {System.out.println("�������� ������ � ��������� ������");}
}
class RuTachpad implements Tachpad{
	@Override
	public void track(int deltaX, int deltaY) {
		int s = (int) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
		System.out.println("������������� �� " + s + "��������");
	}
}
//���������� ���������� �����������
class EngMouse implements Mouse{
	@Override
	public void click() {System.out.println("Click mouse");}
	@Override
	public void dblclick() {System.out.println("Double click mouse");}
	@Override
	public void scroll(int derection) {	
		if(derection>0)
			System.out.println("Scroll Up");
		if(derection<0)
			System.out.println("Scroll dawn");
		if(derection==0)
			System.out.println("No scroll");
	}
}
class EngKeyboard implements Keyboard{
	@Override
	public void print() {System.out.println("Print string");}
	@Override
	public void println() {System.out.println("Print string and go to the next line");}
}
class EngTachpad implements Tachpad{
	@Override
	public void track(int deltaX, int deltaY) {
		int s = (int) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
		System.out.println("Move on " + s + "pixceles");
	}
}
