package ru.adapter;

public class AdapterExample {

	public static void main(String[] args) {
		//������ ������� ������� ������� ������ 
		//�� ������(�� � ���� ���������� ������)
		//� ������(�� � ���� ����������)
		//���� � �������� ������ �.�. ��������� ����������/������� �� ��������� �������

		//1-� ������ ����� ������������
		VectorGrathicsInterfase grathicsInterfase = new VectorAdapterFromRaster();
		grathicsInterfase.drawLine();
		grathicsInterfase.drawSquare();
		
		
		//2-� ������ ����� ����������
		VectorGrathicsInterfase grathicsInterfase2 = new VectorAdapterFromRaster2();
		grathicsInterfase2.drawLine();
		grathicsInterfase2.drawSquare();
		
		//2-� ������ ����� ���������� + ������ ���������������
		VectorGrathicsInterfase grathicsInterfase3 = new VectorAdapterFromRaster3(new RasterGrathics());
		grathicsInterfase3.drawLine();
		grathicsInterfase3.drawSquare();
	}

}

//1-� ������ ����� ������������
//������
interface VectorGrathicsInterfase{
	void drawLine();
	void drawSquare();
}
//������
class RasterGrathics{
	void drawRusterLine() {
		System.out.println("������ �����");
	}
	void drawRusterSquare() {
		System.out.println("������ �������");
	}
}
//�������
class VectorAdapterFromRaster extends RasterGrathics implements VectorGrathicsInterfase{
	//��� �������� ������� ������
	public void drawLine() {
		drawRusterLine();
	}
	//��� �������� ������� ������
	public void drawSquare() {
		drawRusterSquare();
	}
}

//2-� ������ ����� ����������
class VectorAdapterFromRaster2 implements VectorGrathicsInterfase{
	RasterGrathics grathics = new RasterGrathics();
	
	//��� �������� ������� ������
	public void drawLine() {
		grathics.drawRusterLine();
	}
	//��� �������� ������� ������
	public void drawSquare() {
		grathics.drawRusterSquare();
	}
}

//2-� ������ ����� ���������� + ������������� ��� ���������������
class VectorAdapterFromRaster3 implements VectorGrathicsInterfase{
	RasterGrathics grathics;
	public VectorAdapterFromRaster3(RasterGrathics grathics) {
		this.grathics = grathics;
	}
	//��� �������� ������� ������
	public void drawLine() {
		grathics.drawRusterLine();
	}
	//��� �������� ������� ������
	public void drawSquare() {
		grathics.drawRusterSquare();
	}
}
