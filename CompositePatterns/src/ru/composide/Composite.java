package ru.composide;

import java.util.ArrayList;
import java.util.List;

public class Composite {

	//����� � ����� � ����� �������� ���� � �����, 
	//�� �� ���������� ����� ����� ����� ���� ����� � ��������
	
	
	public static void main(String[] args) {
		Shape square1 = new Squere();
		Shape square2 = new Squere();
		Shape triangle = new Triangle();
		
		Shape square3 = new Squere();
		Shape circle1 = new Circle();
		Shape circle2 = new Circle();
		Shape circle3 = new Circle();
		
		CompositeExample composite1 = new CompositeExample();
		CompositeExample composite2 = new CompositeExample();
		CompositeExample composite3 = new CompositeExample();
		
		composite1.addComponent(square1);
		composite1.addComponent(square2);
		composite1.addComponent(triangle);
		
		composite2.addComponent(square3);
		composite2.addComponent(circle1);
		composite2.addComponent(circle2);
		composite2.addComponent(circle3);
		
		composite3.addComponent(composite1);
		composite3.addComponent(composite2);
		composite3.addComponent(new Triangle());
		composite3.draw();
	}
}

interface Shape{
	void draw();
}

class Squere implements Shape{
	public void draw() {
		System.out.println("�������");
	}
}
class Triangle implements Shape{
	public void draw() {
		System.out.println("�����������");
	}
}
class Circle implements Shape{
	public void draw() {
		System.out.println("����");
	}
}


//��� � ���� ��� ������� ��������
//����� ������� ��� ������� ���������
//� ���� ����� ������� ���� ������
class CompositeExample implements Shape{
	
	//��������� ��� �������� ����������
	private List<Shape> components = new ArrayList<>();
	//����� ����������
	public void addComponent(Shape component) {
		components.add(component);
	}
	//����� ��������
	public void remuveComponent(Shape component) {
		components.remove(component);
	}
	//����� ������� ����� � ���������
	//������ ������ �� ������� ���� ����������� � �������� ������� �����
	public void draw() {
		//��������� �������� ����� ���� � ���� �����������
		//��� ������ ������� �� ���� ������� �����������
		//� ������� ����� ���� � ������� ���������� ��� ����������� �� ����� ������ �� �����
		for (Shape component : components) {
			component.draw();
		}
	}
}





