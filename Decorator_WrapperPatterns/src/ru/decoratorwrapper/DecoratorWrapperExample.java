package ru.decoratorwrapper;

public class DecoratorWrapperExample {
	public static void main(String[] args) {
		//����� �� �������� � �� ������� (+ ������� ���� ��� ����) �� ���� � ����� ��������� 
		//��������� �������������� ���������� �������� ����������� �������� ������
		
		PrintInterface interface1 = new Printer("Hello!!!");
		interface1.print();
		
		PrintInterface interface2  = new QuotesDecorator(new Printer("������!"));
		interface2.print();
		
		PrintInterface interface3  = new LeftBrackerDecorator(new Printer("������!"));
		interface3.print();
		
		//������ ������������� ���������� �����������
		//����� ������ �� ��������
		//����� ����� ���������� ������ �� ���� ������� �����������
		//� ����� ��������������� ����������� �� ������ ��������� ������
		//��� � 1-� ��� �� 2-� ������ ����� ���� � �����
		PrintInterface interface4  = new QuotesDecorator(new LeftBrackerDecorator(new RightBrackerDecorator(new Printer("������!"))));
		interface4.print();
		
		//1-� ������ ������������� ������������ ����� ������� ������ ���� ���������
		//2-� ������ (� ����������� �������) ����� ����������� ���������
	}
}


interface PrintInterface{
	void print();
}
//������� �����
class Printer implements PrintInterface{
	String value;
	public Printer(String value) {
		super();
		this.value = value;
	}
	public void print() {
		System.out.print(value);
	}
}

//���������/�������

//������ 2 � ����������� �������(����������� ���)

//������� ��� ���� ����� �� ������ implements PrintInterface
//�� ������ ������ ��������
//� �� ����� ���������� ��� PrintInterface component; �� ��� ��������
abstract class Decorator implements PrintInterface{
	//���������� ������ �� ��������� � ���������
	PrintInterface component;
	public Decorator(PrintInterface component) {
		super();
		this.component = component;
	}
	public void print() {
		component.print();
	}
}

class QuotesDecorator extends Decorator{
	//���� ��������� ������ �� ������� �����(���������)
	public QuotesDecorator(PrintInterface component) {
		super(component);
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		System.out.print("\"");
		super.print();
		System.out.print("\"");
	}
}

//������ ���������� ����������
//����� ������ �� �������� 
class LeftBrackerDecorator extends Decorator{
	//���� ��������� ������ �� ������� �����(���������)
	public LeftBrackerDecorator(PrintInterface component) {
		super(component);
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		System.out.print("[");
		super.print();
	}
}

class RightBrackerDecorator extends Decorator{
	//���� ��������� ������ �� ������� �����(���������)
	public RightBrackerDecorator(PrintInterface component) {
		super(component);
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		super.print();
		System.out.print("]");
	}
}

/*
//1-� ������ ��� ������������ ������ ��� �����������
 * 
class QuotesDecorator implements PrintInterface{
	//���������� ������ �� ��������� � ���������
	PrintInterface component;
	//���� ��������� ������ �� ������� �����(���������)
	public QuotesDecorator(PrintInterface component) {
		this.component = component;
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		System.out.print("\"");
		component.print();
		System.out.print("\"");
	}
}

//������ ���������� ����������
//����� ������ �� �������� 
class LeftBrackerDecorator implements PrintInterface{
	//���������� ������ �� ��������� � ���������
	PrintInterface component;
	//���� ��������� ������ �� ������� �����(���������)
	public LeftBrackerDecorator(PrintInterface component) {
		this.component = component;
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		System.out.print("[");
		component.print();
	}
}

class RightBrackerDecorator implements PrintInterface{
	//���������� ������ �� ��������� � ���������
	PrintInterface component;
	//���� ��������� ������ �� ������� �����(���������)
	public RightBrackerDecorator(PrintInterface component) {
		this.component = component;
	}
	//������ ������ ����� �� ������ �������������
	public void print() {
		
		component.print();
		System.out.print("]");
	}
}

*/
