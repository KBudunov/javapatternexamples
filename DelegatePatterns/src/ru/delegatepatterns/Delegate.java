package ru.delegatepatterns;

public class Delegate {

	public static void main(String[] args) {
		//������ �������� �� �#
		//��� ������ ��������, �������� � ������ ��������� ����
		
		//������� 1
		B b = new B();
		b.f();
		
		//������� 2
		Painter painter = new Painter();
		painter.setGraphics(new Triangle());
		painter.draw();
		painter.setGraphics(new Cyrcle());
		painter.draw();
	}
}

//������� 1
class A {
	void f(){
		System.out.println("f()");
		/*
		 * ����� ����
		 */
	}
}
class B{
	A a = new A();
	void f() {
		a.f();
	}
}


//������� 2
interface Graphics{
	void draw();
}
//������������
class Triangle implements Graphics{
	@Override
	public void draw() {
		System.out.println("Triangle");
	}
}
class Rectangle implements Graphics{
	@Override
	public void draw() {
		System.out.println("Rectangle");
	}
}
class Cyrcle implements Graphics{
	@Override
	public void draw() {
		System.out.println("Cyrcle");
	}
}

//������������
class Painter {
	Graphics graphics;
	//����� ������� (��������� �������� ��� ��������)
	public void setGraphics(Graphics g) {
		graphics = g;
	}
	public void draw() {
		graphics.draw();
	}
}



