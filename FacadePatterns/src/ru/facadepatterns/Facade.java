package ru.facadepatterns;

public class Facade {
	public static void main(String[] args) {
		//������ ��� ������������� ������
		Power power = new Power();
		DVDRom dvdRom = new DVDRom();
		HDD hdd = new HDD();
		power.on();
		dvdRom.load();
		//dvdRom.unload();
		hdd.copyFromDVD(dvdRom);
		
		//������������� ������
		Computer computer = new Computer();
		computer.copy(); //������ ������� �� ����� �������
	}
	/*
	 * ����� �������� �� ����� ����� ��������� (�.�. ����� ������� �� �����)
	 * � ������� �������� ���� ����� �� �����
	 * */
}


class Computer{ //�����
	//������ ��� ������������� ������
	Power power = new Power();
	DVDRom dvdRom = new DVDRom();
	HDD hdd = new HDD();
	
	public void copy() { 
		power.on();
		dvdRom.load();
		hdd.copyFromDVD(dvdRom);
	}
}


class Power{
	void on() {
		System.out.println("��������� �������");
	}
	void off() {
		System.out.println("���������� �������");
	}
}

class DVDRom{
	private boolean date = false;
	public boolean hasData() {
		return date;
	}
	void load() {
		date = true;
	}
	void unload() {
		date = false;
	}
}

class HDD{
	void copyFromDVD(DVDRom dvd){
		if(dvd.hasData()){
			System.out.println("���������� ����������� ������ � �����");
		} else {
			System.out.println("�������� ���� � �������");
		}
			
	}
}