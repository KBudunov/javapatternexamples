package ru.factorymethod;

import java.util.Date;

public class FactoryMethod {

	public static void main(String[] args) {
		//��������� ����� == ������� ���� ���� �������
		//����������� ������� == ������� ���� ������ ���������
		
		//����� ��� �������� ���������� ������
		//�������� �������������
		//WatchMaker maker = new DigitalWatchMeker();//new RomeWatchMeker();
		WatchMaker maker = getMakerByName("Rome"); //��� ������ ��������
		//���� ��� �������� ���� � ����� � ����� ������
		Watch watch = maker.createWatch();
		watch.showTime();
	}
	
	//�������������, ��� ������ ��������
	private static WatchMaker getMakerByName(String maker) {
		if("Digital".equals(maker)){
			return new DigitalWatchMeker();
		}
		if("Rome".equals(maker)){
			return new RomeWatchMeker();
		}
		throw new RuntimeException("�� �������������� ���������������� ����� �����: " + maker);
	}
}

//��������� ��������
interface Watch{
	void showTime();
}
class DigitalWatch implements Watch{
	@Override
	public void showTime() {
		System.out.println(new Date());
	}
}
class RomelWatch implements Watch{
	@Override
	public void showTime() {
		System.out.println("XXI-IIV");
	}
}



//��������� �������
interface WatchMaker{
	//��� � ���� ��������� �����
	Watch createWatch();
}

class DigitalWatchMeker implements WatchMaker{
	//��� ��������� �����
	public Watch createWatch() {
		return new DigitalWatch(); //��������� has a
	}
}

class RomeWatchMeker implements WatchMaker{
	//��� ��������� �����
	public Watch createWatch() {
		return new RomelWatch();  //��������� has a
	}
}

