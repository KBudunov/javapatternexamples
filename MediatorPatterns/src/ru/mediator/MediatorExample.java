package ru.mediator;

import java.util.ArrayList;
import java.util.List;

public class MediatorExample {

	public static void main(String[] args) {
		TextChat chat = new TextChat();
		User admin =  new Admin(chat, "Ivan (Admin)");
		User u1 = new SimpleUser(chat, "Alex");
		User u2 = new SimpleUser(chat, "Petr");
		
		chat.setAdmin(admin);
		chat.addUser(u1);
		chat.addUser(u2);
		
		u1.sendMessage("������ ���� ����� Alex");
		admin.sendMessage("����� ����� � ���");
	}
}

//������
interface User{
	void sendMessage(String message);
	void getMessage(String message);
}

class Admin implements User{
	String name;
	Chat chat;
	public Admin(Chat chat, String name) {this.chat = chat; this.name = name;}
	//��������
	@Override
	public void sendMessage(String message) {
		chat.sendMessage(message, this);
	}
	//����������
	@Override
	public void getMessage(String message) {
		System.out.println("������������� " + name +" �������� ���������: " + message);
	}
}

class SimpleUser implements User{
	Chat chat;
	String name;
	public SimpleUser(Chat chat, String name) {this.chat = chat; this.name = name;}
	//��������
	@Override
	public void sendMessage(String message) {
		chat.sendMessage(message, this);
	}
	//����������
	@Override
	public void getMessage(String message) {
		System.out.println("������������ " + name +" �������� ���������: " + message);
	}
}

//��������
interface Chat{
	void sendMessage(String message, User user);
}

class TextChat implements Chat{
	User admin;
	List<User> users = new ArrayList<>();
	
	public void setAdmin(User admin) {
		this.admin = admin;
	}
	
	public void addUser(User user) {
		users.add(user);
	}
	
	//������ �������� ���������
	//(String message - ���������, User user - ��� ��������)
	@Override
	public void sendMessage(String message, User user) {
		for (User u : users) {
			u.getMessage(message);
		}
		admin.getMessage(message);
	}
}



