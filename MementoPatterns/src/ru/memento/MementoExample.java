package ru.memento;

public class MementoExample {

	public static void main(String[] args) {
		//������������ ��� �������� ��������� �������
		
		
		//�������� ����
		Game game = new Game();
		game.set("LVL 1", 30000);
		System.out.println(game);
		
		//�������� ���������� � ���������
		File file = new File();
		file.setSave(game.save());
		
		//�������� ����
		game.set("LVL 2", 80000);
		System.out.println(game);
		
		//��������� ����
		game.load(file.getSave());
		System.out.println(game);
		
	}
}

//���������
class Game{
	private int ms;
	private String level;
	public void set(String level, int ms) {
		this.level = level;
		this.ms = ms;
	}
	public void load(Save save) {
		ms = save.getMS();
		level = save.getLEVEL();
	}
	public Save save() {
		return new Save(ms, level);
	}
	@Override
	public String toString() {
		return "Game, level: " + level + ", ms: " + ms;
	}
}

//���������
class Save{
	private final int MS;
	private final String LEVEL;
	
	public Save(int mS, String lEVEL) {
		super();
		MS = mS;
		LEVEL = lEVEL;
	}
	public int getMS() {return MS;}
	public String getLEVEL() {return LEVEL;}
}

//������
class File{
	Save save;
	public Save getSave() {return save;}
	public void setSave(Save save) {this.save = save;}
}
















