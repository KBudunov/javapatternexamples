package ru.observer;

import java.util.ArrayList;
import java.util.List;

public class ObserverExample {
	public static void main(String[] args) {
		//������ ��������� ������ ��������/���������
		Meteostation meteostation = new Meteostation();
		meteostation.addObserver(new ConsoleObserver());
		meteostation.addObserver(new FileObserver());
		
		meteostation.setMeasurements(100, 12);
		
	}
}

//��������
interface Observable{
	void addObserver(Observer o);
	void removeObserver(Observer o);
	void notifyObservers();
}

class Meteostation implements Observable{
	int temperature;
	int pressere;
	List<Observer>  observers = new ArrayList<>();
	
	public void setMeasurements(int t, int p) {
		temperature = t;
		pressere = p;
		notifyObservers();
	}
	
	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}
	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}
	@Override
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.handleEvent(temperature, pressere);
		}
	}
}




//���������
interface Observer{
	void handleEvent(int temp, int presser);
}
class ConsoleObserver implements Observer{
	@Override
	public void handleEvent(int temp, int presser) {
		System.out.println("Console. ��������� ������. ����������� = " + temp + ", �������� = " + presser);
	}
}

class FileObserver implements Observer{
	@Override
	public void handleEvent(int temp, int presser) {
		System.out.println("File. ��������� ������. ����������� = " + temp + ", �������� = " + presser);
	}
}



