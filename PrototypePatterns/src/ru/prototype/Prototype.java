package ru.prototype;

public class Prototype {

	public static void main(String[] args) {
		//������ ������ ��� ���������� � ���� � ���������� Clonable
		Human original = new Human(18, "Ivan");
		System.out.println(original.toString());
		
		Human copy = (Human)original.copy();
		System.out.println(copy.toString());
		
		
		HumanFactory humanFactory = new HumanFactory(copy);
		Human h1 = humanFactory.makeCopy();
		System.out.println(h1);
		
		humanFactory.setPrototype(new Human(29, "Elen"));
		Human h2 = humanFactory.makeCopy();
		System.out.println(h2);
		
	}
}

interface Copyble{
	//������ ������ ���� ��� ���� ����� ���� ��������� 
	//��� ������������� � �������� ���������� ��� ��� ������
	Object copy();
}

class Human implements Copyble{
	int age;
	String name;
	public Human(int age, String name) {this.age = age;	this.name = name;}
	public Object copy() {Human copy = new Human(age, name);return copy;}
	public String toString() {return "���: " + name + " , �������: " + age;}
}


class HumanFactory{
	Human human;
	
	public HumanFactory(Human human) {
		setPrototype(human); 
	}
	
	public void setPrototype(Human human) {
		this.human = human;
	}
	Human makeCopy(){
		return (Human) human.copy();
	}
}

